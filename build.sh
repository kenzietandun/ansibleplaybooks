#!/bin/bash

DIR="$(pwd)"
sed "s%ANSIBLE_DIR%$DIR%g" ansible.cfg.backup > ansible.cfg
if [[ -f "${HOME}"/.ansible.cfg ]]
then
  rm "${HOME}"/.ansible.cfg
  ln -s "${DIR}"/ansible.cfg "${HOME}"/.ansible.cfg
fi
